**Задание 4:**
- Создать проект используя Spring Boot Initializr
- Перенести приложение проведения опросов.
- Перенести все свойства в application.yml
- Сделать собственный баннер для приложения.
- Перенести тесты и использовать spring boot test starter

**Отладка:**
- Program arguments: --debug для просмота подключенных конфигураций (CONDITIONS EVALUATION REPORT)