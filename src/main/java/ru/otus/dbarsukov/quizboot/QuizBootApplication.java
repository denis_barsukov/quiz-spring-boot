package ru.otus.dbarsukov.quizboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ru.otus.dbarsukov.quizboot.domain.Quiz;
import ru.otus.dbarsukov.quizboot.service.QuizService;

@SpringBootApplication
public class QuizBootApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context =
                SpringApplication.run(QuizBootApplication.class, args);

		QuizService service = context.getBean(QuizService.class);
        Quiz quiz = service.getDefault();
        quiz.start();
    }

}
