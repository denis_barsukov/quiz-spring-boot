package ru.otus.dbarsukov.quizboot.dao;

import ru.otus.dbarsukov.quizboot.domain.QuestionAndAnswers;

import java.util.List;

public interface QuestionAndAnswersDao {
    List<QuestionAndAnswers> getQuestionAndAnswers(String name);
}
