package ru.otus.dbarsukov.quizboot.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class QuestionAndAnswers {
    private final Integer id;
    private final String question;
    private final List<String> answers;
    private final Integer correctAnswerId;
}
