package ru.otus.dbarsukov.quizboot.domain;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import ru.otus.dbarsukov.quizboot.util.ConsoleIO;

import java.util.List;
import java.util.Locale;

@Slf4j
@RequiredArgsConstructor
public class Quiz {
    private final Person person;
    private final List<QuestionAndAnswers> questionAndAnswers;
    private final MessageSource messageSource;
    private final Integer quizThreshold;

    private Integer correctAnswersNumber = 0;

    public void start() {
        if (questionAndAnswers.isEmpty()) {
            System.out.println(messageSource.getMessage("error.message",
                    null,
                    Locale.getDefault()));
            return;
        }

        person.introduceYourself();
        askQuestions();
        printResults();
        saveResult();
    }

    private void saveResult() {
        if (correctAnswersNumber >= quizThreshold) {
            System.out.println(messageSource.getMessage("passed.message",
                    null,
                    Locale.getDefault()));
            log.info("Person '{} {}' passed quiz with score {} of {}", person.getFirstName(),
                    person.getSurname(), correctAnswersNumber, questionAndAnswers.size());
        }
        else {
            System.out.println(messageSource.getMessage("failed.message",
                    null,
                    Locale.getDefault()));
            log.info("Person '{} {}' failed quiz with score {} of {}", person.getFirstName(),
                    person.getSurname(), correctAnswersNumber, questionAndAnswers.size());
        }
    }

    private void askQuestions() {
        for (QuestionAndAnswers current : questionAndAnswers) {
            printQuestion(current);
            printAnswers(current);
            if (isAnswerCorrect(current)) {
               correctAnswersNumber++;
            }
        }
    }

    private void printAnswers(QuestionAndAnswers current) {
        int answerNo = 1;
        for(String answer : current.getAnswers()) {
            System.out.println(answerNo++ + ". " + answer);
        }
    }

    private boolean isAnswerCorrect(QuestionAndAnswers current) {
        int choice = ConsoleIO.readInteger();
        return choice == current.getCorrectAnswerId();
    }

    private void printQuestion(QuestionAndAnswers current) {
        System.out.print(messageSource.getMessage("question.number",
                new Integer[] {current.getId()},
                Locale.getDefault()));
        System.out.println(current.getQuestion());
    }

    private void printResults() {
        System.out.println(messageSource.getMessage("correct.answers",
                new Integer[] {correctAnswersNumber, questionAndAnswers.size()},
                Locale.getDefault()));
    }
}
