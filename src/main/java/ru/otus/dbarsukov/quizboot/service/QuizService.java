package ru.otus.dbarsukov.quizboot.service;

import ru.otus.dbarsukov.quizboot.domain.Quiz;

public interface QuizService {
    Quiz getByName(String name);
    Quiz getDefault();
}
