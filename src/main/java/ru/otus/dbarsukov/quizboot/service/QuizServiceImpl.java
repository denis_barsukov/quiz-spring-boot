package ru.otus.dbarsukov.quizboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.otus.dbarsukov.quizboot.config.ApplicationProperties;
import ru.otus.dbarsukov.quizboot.dao.QuestionAndAnswersDao;
import ru.otus.dbarsukov.quizboot.domain.Person;
import ru.otus.dbarsukov.quizboot.domain.Quiz;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class QuizServiceImpl implements QuizService {

    private final Person person;
    private final QuestionAndAnswersDao dao;
    private final MessageSource messageSource;
    private final ApplicationProperties settings;

    public Quiz getByName(String name) {
        String fullName = name + '_' + Locale.getDefault();
        return new Quiz(person, dao.getQuestionAndAnswers(fullName), messageSource, settings.getThreshold());
    }

    public Quiz getDefault() {
        return getByName(settings.getDefaultQuizName());
    }
}
