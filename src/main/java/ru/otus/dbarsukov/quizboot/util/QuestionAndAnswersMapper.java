package ru.otus.dbarsukov.quizboot.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.otus.dbarsukov.quizboot.domain.QuestionAndAnswers;
import ru.otus.dbarsukov.quizboot.dto.QuestionAndAnswersDto;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class QuestionAndAnswersMapper {

    public static QuestionAndAnswers convert(QuestionAndAnswersDto dto) {
        return new QuestionAndAnswers(dto.getId(), dto.getQuestion(),
                questionsToList(dto), dto.getCorrectAnswerId());
    }

    private static List<String> questionsToList(QuestionAndAnswersDto dto) {
        return Arrays.asList(dto.getAnswer1(), dto.getAnswer2(),
                dto.getAnswer3(), dto.getAnswer4());
    }
}
