package ru.otus.dbarsukov.quizboot;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AbstractTest {
    protected final String NAME = "foo";
    protected final String SURNAME = "bar";

    protected final Integer ID = 1;
    protected final String QUESTION = "?";
    protected final List<String> ANSWERS = Arrays.asList("a", "b", "c", "d");
    protected final Integer CORRECT_ANSWER = 4;

    protected final InputStream defaultSystemIn = System.in;
    protected final PrintStream defaultSystemOut = System.out;

    private static final Locale defaultLocale = Locale.getDefault();
    private static final Locale enLocale = new Locale("en", "EN");

    @BeforeClass
    public static void setEnLocale() {
        Locale.setDefault(enLocale);
    }

    @AfterClass
    public static void setDefaultLocale() {
        Locale.setDefault(defaultLocale);
    }
}
