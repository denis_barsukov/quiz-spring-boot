package ru.otus.dbarsukov.quizboot.dao;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;
import ru.otus.dbarsukov.quizboot.AbstractTest;
import ru.otus.dbarsukov.quizboot.domain.QuestionAndAnswers;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class QuestionAndAnswersDaoCsvImplTest extends AbstractTest {

    @Mock
    private Appender mockAppender;

    @Test
    public void noSuchCsvFileLoggingTest() {
        Logger logger = (Logger)LoggerFactory.getLogger(QuestionAndAnswersDaoCsvImpl.class);
        logger.addAppender(mockAppender);

        ArgumentCaptor<LoggingEvent> eventArgumentCaptor = ArgumentCaptor.forClass(LoggingEvent.class);

        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        dao.getQuestionAndAnswers("no_such_file");

        verify(mockAppender, times(1)).doAppend(eventArgumentCaptor.capture());

        assertEquals("Can't load quiz file!", eventArgumentCaptor.getAllValues().get(0).getMessage());
        assertEquals(Level.ERROR, eventArgumentCaptor.getAllValues().get(0).getLevel());
    }

    @Test
    public void getQuestionAndAnswers() {
        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        List<QuestionAndAnswers> questionAndAnswers = dao.getQuestionAndAnswers("test_en_EN");

        assertEquals(1, questionAndAnswers.size());
        assertEquals(ID, questionAndAnswers.get(0).getId());
        assertEquals(QUESTION, questionAndAnswers.get(0).getQuestion());
        assertEquals(ANSWERS, questionAndAnswers.get(0).getAnswers());
        assertEquals(CORRECT_ANSWER, questionAndAnswers.get(0).getCorrectAnswerId());
    }

    @Test
    public void wrongCsvFormat() {
        Logger logger = (Logger)LoggerFactory.getLogger(QuestionAndAnswersDaoCsvImpl.class);
        logger.addAppender(mockAppender);

        ArgumentCaptor<LoggingEvent> eventArgumentCaptor = ArgumentCaptor.forClass(LoggingEvent.class);

        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        List<QuestionAndAnswers> questionAndAnswers = dao.getQuestionAndAnswers("wrongFormat_en_EN");

        verify(mockAppender, times(1)).doAppend(eventArgumentCaptor.capture());

        assertEquals("Quiz file parsing error!", eventArgumentCaptor.getAllValues().get(0).getMessage());
        assertEquals(Level.ERROR, eventArgumentCaptor.getAllValues().get(0).getLevel());

        assertTrue(questionAndAnswers.isEmpty());
    }

    @Test
    public void emptyCsv() {
        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        List<QuestionAndAnswers> questionAndAnswers = dao.getQuestionAndAnswers("empty");

        assertTrue(questionAndAnswers.isEmpty());
    }

}