package ru.otus.dbarsukov.quizboot.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.otus.dbarsukov.quizboot.AbstractTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonTest extends AbstractTest {

    @Autowired
    private MessageSource messageSource;

    private final String INPUT = NAME + System.lineSeparator() + SURNAME + System.lineSeparator();
    private final String MESSAGE1 = "Name: ";
    private final String MESSAGE2 = "Surname: ";
    private final String OUTPUT = MESSAGE1 + MESSAGE2;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void redirectInputAndOutput() {
        System.setIn(new ByteArrayInputStream(INPUT.getBytes()));
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreInputAndOutput() {
        System.setIn(defaultSystemIn);
        System.setOut(new PrintStream(defaultSystemOut));
    }

    @Test
    public void introduceYourselfInput() {
        Person person = new Person(messageSource);
        person.introduceYourself();

        assertEquals(NAME, person.getFirstName());
        assertEquals(SURNAME, person.getSurname());
    }

    @Test
    public void introduceYourselfOutput() {
        Person person = new Person(messageSource);
        person.introduceYourself();

        assertEquals(OUTPUT, outContent.toString());
    }
}
