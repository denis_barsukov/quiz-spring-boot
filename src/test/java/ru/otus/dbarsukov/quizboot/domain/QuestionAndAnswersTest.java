package ru.otus.dbarsukov.quizboot.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.otus.dbarsukov.quizboot.AbstractTest;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class QuestionAndAnswersTest extends AbstractTest {

    @Test
    public void gettersTest() {
        QuestionAndAnswers questionAndAnswers = new QuestionAndAnswers(ID, QUESTION, ANSWERS, CORRECT_ANSWER);

        assertEquals(ID, questionAndAnswers.getId());
        assertEquals(QUESTION, questionAndAnswers.getQuestion());
        assertEquals(ANSWERS, questionAndAnswers.getAnswers());
        assertEquals(CORRECT_ANSWER, questionAndAnswers.getCorrectAnswerId());
    }

}