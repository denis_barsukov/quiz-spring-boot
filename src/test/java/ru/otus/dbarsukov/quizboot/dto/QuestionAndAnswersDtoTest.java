package ru.otus.dbarsukov.quizboot.dto;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.google.code.beanmatchers.BeanMatchers.*;

@RunWith(JUnit4.class)
public class QuestionAndAnswersDtoTest {

    @Test
    public void testQuestionAndAnswersDto() {
        MatcherAssert.assertThat(QuestionAndAnswersDto.class, CoreMatchers.allOf(
                hasValidBeanConstructor(),
                hasValidGettersAndSetters(),
                hasValidBeanHashCode(),
                hasValidBeanEquals(),
                hasValidBeanToString()
        ));
    }
}