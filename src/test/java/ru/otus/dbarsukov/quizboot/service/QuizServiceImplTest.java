package ru.otus.dbarsukov.quizboot.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.otus.dbarsukov.quizboot.AbstractTest;
import ru.otus.dbarsukov.quizboot.config.ApplicationProperties;
import ru.otus.dbarsukov.quizboot.dao.QuestionAndAnswersDao;
import ru.otus.dbarsukov.quizboot.domain.Person;
import ru.otus.dbarsukov.quizboot.domain.QuestionAndAnswers;

import java.io.ByteArrayInputStream;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QuizServiceImplTest extends AbstractTest {

    private final String ANSWER = "4" + System.lineSeparator();

    @Mock
    private Person mockPerson;

    @Mock
    private QuestionAndAnswersDao mockDao;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ApplicationProperties settings;

    @Before
    public void redirectInput() {
        System.setIn(new ByteArrayInputStream(ANSWER.getBytes()));
    }

    @After
    public void restoreInputAndOutput() {
        System.setIn(defaultSystemIn);
    }

    @Test
    public void getByName() {
        doNothing().when(mockPerson).introduceYourself();
        when(mockPerson.getFirstName()).thenReturn(NAME);
        when(mockPerson.getSurname()).thenReturn(SURNAME);

        when(mockDao.getQuestionAndAnswers(anyString()))
                .thenReturn(Collections.singletonList(new QuestionAndAnswers(ID, QUESTION, ANSWERS, CORRECT_ANSWER)));

        QuizService quiz = new QuizServiceImpl(mockPerson, mockDao, messageSource, settings);
        quiz.getByName("baz").start();
    }

}