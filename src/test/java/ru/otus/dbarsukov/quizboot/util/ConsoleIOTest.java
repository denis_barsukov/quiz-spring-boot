package ru.otus.dbarsukov.quizboot.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static ru.otus.dbarsukov.quizboot.util.ConsoleIO.readInteger;

@RunWith(JUnit4.class)
public class ConsoleIOTest {

    private static final String NUMBER = "123";
    private static final String INCORRECT_INPUT = "not integer string" + System.lineSeparator() + NUMBER;
    private static final String OUTPUT = "Not a number! Enter a number or Ctrl-c to exit." + System.lineSeparator();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final InputStream defaultSystemIn = System.in;
    private final PrintStream defaultSystemOut = System.out;

    @Before
    public void redirectInputAndOutput() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreInputAndOutput() {
        System.setIn(defaultSystemIn);
        System.setOut(new PrintStream(defaultSystemOut));
    }

    @Test
    public void readInt() {
        System.setIn(new ByteArrayInputStream(NUMBER.getBytes()));
        int number = readInteger();

        assertEquals(123, number);
    }

    @Test
    public void readNotInt() {
        System.setIn(new ByteArrayInputStream(INCORRECT_INPUT.getBytes()));
        readInteger();

        assertEquals(OUTPUT, outContent.toString());
    }

}